
#include <cmath>
#include <jni.h>
#include <android/bitmap.h>
#include <android/log.h>

#include "GridGraph_2D_4C.h"

#define MAX_ALPHA 255
#define ALPHA_CHANNEL 0xFF000000
#define RGB_CHANNEL 0x00FFFFFF
#define RED_CHANNEL 0x00FF0000
#define GREEN_CHANNEL 0x0000FF00
#define BLUE_CHANNEL 0x000000FF

#define ABS_COORD(x,y,width) ((y)*(width) + (x))
#define RGB(argb) ((argb) & RGB_CHANNEL)
#define ALPHA(argb) (((argb) & ALPHA_CHANNEL) >> 24)

extern "C"
{
JNIEXPORT jint JNICALL
Java_brezimi4_fel_cvut_cz_imageprocessingtest_NativeLib_reverseRedBlue(JNIEnv*,jobject,jobject);
JNIEXPORT jobject JNICALL
Java_brezimi4_fel_cvut_cz_imageprocessingtest_NativeLib_colorImage(JNIEnv*,jobject,jobject,jobject,jobject,jintArray);
}

void segmentImage(unsigned,unsigned,uint32_t,float*,uint32_t*,uint8_t*,uint32_t*);
short compute_weight(float,float,unsigned);
int argb_to_grey(uint32_t*,float*,int);
uint32_t change_pixel_contrast(uint32_t, float*);
uint32_t get_opaque_color(uint32_t);
short compute_terminal_cap(uint8_t,unsigned);


JNIEXPORT jint JNICALL
Java_brezimi4_fel_cvut_cz_imageprocessingtest_NativeLib_reverseRedBlue(JNIEnv *env, jobject,
                                                                       jobject jbitmap) {
    AndroidBitmapInfo info;
    void* pixels;

    AndroidBitmap_getInfo(env, jbitmap, &info);
    AndroidBitmap_lockPixels(env, jbitmap, &pixels);
    uint32_t* bitmapPixels = (uint32_t*) pixels;

    int pixelCount = info.width*info.height;
    for (int index = 0; index < pixelCount; index++) {
        uint32_t* p = bitmapPixels + index;
        *p = (*p & 0xFF00FF00) | ((*p & RED_CHANNEL) >> 16) | ((*p & BLUE_CHANNEL) << 16); //red and blue switch
        //*p = (*p << 8) + ((*p & 0xF000) >> 32); //alpha and blue switch
    }

    printf("%d\n", info.format);
    AndroidBitmap_unlockPixels(env, jbitmap);
    return 0;
}

JNIEXPORT jobject JNICALL
Java_brezimi4_fel_cvut_cz_imageprocessingtest_NativeLib_colorImage(JNIEnv *env, jobject,
                                                                       jobject jbitmap, jobject jscribbles, jobject jalpha, jintArray jlables) {
    AndroidBitmapInfo bitmapInfo;
    void* bitmap;
    AndroidBitmapInfo scribblesInfo;
    void* scribbles;
    AndroidBitmapInfo alphaInfo;
    void* alpha;

    //collect bitmap and scribbles info
    AndroidBitmap_getInfo(env, jbitmap, &bitmapInfo);
    AndroidBitmap_getInfo(env, jscribbles, &scribblesInfo);
    AndroidBitmap_getInfo(env, jalpha, &alphaInfo);
    if ((bitmapInfo.width != scribblesInfo.width) || (bitmapInfo.height != scribblesInfo.height)
        || (bitmapInfo.format != ANDROID_BITMAP_FORMAT_RGBA_8888) || (scribblesInfo.format != ANDROID_BITMAP_FORMAT_RGBA_8888)) {
        return NULL;
    }

    //access jlables
    int* lables;
    int lablesLength;
    lables = env->GetIntArrayElements(jlables, NULL);
    lablesLength = env->GetArrayLength(jlables);


    unsigned width = bitmapInfo.width;
    unsigned height = bitmapInfo.height;

    //create grey scale bitmap, lock pixels
    AndroidBitmap_lockPixels(env, jbitmap, &bitmap);
    uint32_t* bitmapPixels = (uint32_t*) bitmap;
    float* gBitmapPixels = (float*) malloc(width*height * sizeof(*gBitmapPixels));
    argb_to_grey(bitmapPixels, gBitmapPixels, width*height);
    AndroidBitmap_lockPixels(env, jscribbles, &scribbles);
    uint32_t* scribblesPixels = (uint32_t*) scribbles;
    AndroidBitmap_lockPixels(env, jalpha, &alpha);
    uint8_t* alphaPixels = (uint8_t*) alpha;

    uint32_t* bitmapMask = (uint32_t*) calloc(width*height, sizeof(*bitmapMask));

    for (int lableIndex = 0; lableIndex < lablesLength; lableIndex++) {
        segmentImage(width, height, lables[lableIndex], gBitmapPixels, scribblesPixels, alphaPixels, bitmapMask);
    }

    jclass bitmapCls = env->GetObjectClass(jbitmap);
    jmethodID createBitmapFunction = env->GetStaticMethodID(bitmapCls, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    jstring configName = env->NewStringUTF("ARGB_8888");
    jclass bitmapConfigClass = env->FindClass("android/graphics/Bitmap$Config");
    jmethodID valueOfBitmapConfigFunction = env->GetStaticMethodID(bitmapConfigClass, "valueOf", "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;");
    jobject bitmapConfig = env->CallStaticObjectMethod(bitmapConfigClass, valueOfBitmapConfigFunction, configName);
    jobject jsegmentedBitmap = env->CallStaticObjectMethod(bitmapCls, createBitmapFunction, width, height, bitmapConfig);
    void* segmentedBitmap;
    AndroidBitmap_lockPixels(env, jsegmentedBitmap, &segmentedBitmap);
    uint32_t* segmentedBitmapPixels = (uint32_t*) segmentedBitmap;

    for (int y=0;y<height;y++)
    {
        for (int x=0;x<width;x++)
        {
            segmentedBitmapPixels[ABS_COORD(x,y,width)] = change_pixel_contrast(bitmapMask[ABS_COORD(x,y,width)],
                                                                                gBitmapPixels + ABS_COORD(x,y,width));
        }
    }

    AndroidBitmap_unlockPixels(env, jbitmap);
    AndroidBitmap_unlockPixels(env, jscribbles);
    AndroidBitmap_unlockPixels(env, jalpha);
    AndroidBitmap_unlockPixels(env, jsegmentedBitmap);
    free(gBitmapPixels);
    free(bitmapMask);

    return jsegmentedBitmap;
}

#define MIN(A,B) ((A) < (B) ? (A) : (B))
#define TERMINAL_CAP(B,K) ((short) ((B == 1) ? (0.05f*(K)) : (K)))
#define WHITE 0xFFFFFFFF

void segmentImage(unsigned width, unsigned height, uint32_t lable,
                  float* gBitmapPixels, uint32_t* scribblesPixels, uint8_t* alphaPixels, uint32_t* bitmapMask) {

    unsigned coef = 2*(width + height);
    typedef GridGraph_2D_4C<short,short,int> Grid;
    Grid* grid = new Grid(width,height);

    int softCount = 0;
    int hardCount = 0;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if(bitmapMask[ABS_COORD(x,y,width)] != 0) continue;

            uint32_t curScribblePixel = scribblesPixels[ABS_COORD(x,y,width)];
            grid->set_terminal_cap(grid->node_id(x,y),
                                   ((curScribblePixel != WHITE) && (curScribblePixel != lable)) ?
                                   TERMINAL_CAP(alphaPixels[ABS_COORD(x,y,width)], coef) : (short) 0,
                                   (curScribblePixel == lable) ?
                                   TERMINAL_CAP(alphaPixels[ABS_COORD(x,y,width)], coef) : (short) 0);

            if (alphaPixels[ABS_COORD(x,y,width)] == 1) {
                softCount++;
            } else if (alphaPixels[ABS_COORD(x,y,width)] == 2) {
                hardCount++;
            }


            if (x<width-1 && bitmapMask[ABS_COORD(x+1,y,width)] == 0)
            {
                const short cap = compute_weight(gBitmapPixels[ABS_COORD(x,y,width)], gBitmapPixels[ABS_COORD(x+1,y,width)], coef);

                grid->set_neighbor_cap(grid->node_id(x  ,y),+1,0,cap);
                grid->set_neighbor_cap(grid->node_id(x+1,y),-1,0,cap);
            }

            if (y<height-1 && bitmapMask[ABS_COORD(x,y+1,width)] == 0)
            {
                const short cap = compute_weight(gBitmapPixels[ABS_COORD(x,y,width)], gBitmapPixels[ABS_COORD(x,y+1,width)], coef);

                grid->set_neighbor_cap(grid->node_id(x,y  ),0,+1,cap);
                grid->set_neighbor_cap(grid->node_id(x,y+1),0,-1,cap);
            }
        }
    }

    grid->compute_maxflow();

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            if (grid->get_segment(grid->node_id(x,y)))
                bitmapMask[ABS_COORD(x,y,width)] = lable;
        }
    }

    delete grid;
}

short compute_weight(float p_intensity, float q_intensity, unsigned coef) {
    float min = MIN(p_intensity, q_intensity);
    return (short) (1 + coef*min*min);
}

int argb_to_grey(uint32_t* srcPixels, float* destPixels, int size) {
    if (srcPixels == NULL || destPixels == NULL) return -1;

    for (int i = 0; i < size; i++) {
        destPixels[i] = (float) (0.33*((srcPixels[i] & RED_CHANNEL) >> 16) + 0.33*((srcPixels[i] & GREEN_CHANNEL) >> 8) + 0.33*(srcPixels[i] & BLUE_CHANNEL))/255;
    }

    return 0;
}

uint32_t change_pixel_contrast(uint32_t argb, float* contrast) {
    uint32_t  ret = (argb & ALPHA_CHANNEL)
                    + ((uint32_t)((*contrast)*(float)((argb &  RED_CHANNEL) >> 16)) << 16)
                    + ((uint32_t)((*contrast)*(float)((argb & GREEN_CHANNEL) >> 8)) << 8)
                    + (uint32_t)((*contrast)*(float)(argb & BLUE_CHANNEL));

    return ret;
}

uint32_t get_opaque_color(uint32_t color) {
    return ALPHA_CHANNEL + RGB(color);
}

