package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.Color;

import org.w3c.dom.Text;

import java.io.IOException;

import static brezimi4.fel.cvut.cz.imageprocessingtest.ConfirmSelectionDialogFragment.*;

/**
 * <i>GalleryActivity</i> handles user request to browse device storage through Intent.ACTION_GET_CONTENT
 * filtered with "image/*" type then shows selected image's preview. *
 */
public class GalleryActivity extends Activity implements ConfirmDialogListener{
    public static final int PICK_IMAGE = 1;
    ImageView galleryPicImageView;
    Button confirmSelectionButton;
    Imaginator image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        galleryPicImageView = (ImageView) findViewById(R.id.galleryPicImageView);
        galleryPicImageView.setOnLongClickListener(new GalleryPicImageViewLongClicked());
        confirmSelectionButton = (Button) findViewById(R.id.confirmSelectionButton);
        confirmSelectionButton.setOnClickListener(new ConfirmSelectionButton());

        if (getIntent().hasExtra("bitmap_uri")) {
            Uri uri = Uri.parse(getIntent().getExtras().getString("bitmap_uri"));
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                image = new Imaginator(bitmap, this);
                UpdateGalleryPicImageView();
            } catch (IOException e) {
                e.printStackTrace();
                StartBrowseGalleryIntent();
            }
        } else {
            StartBrowseGalleryIntent();
        }
    }

    /**
     * Retrieves Bitmap by uri selected through Intent.ACTION_GET_CONTENT and displays it
     * in galleryPicImageView.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                image = new Imaginator(bitmap, this);
                UpdateGalleryPicImageView();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Restarts Intent.ACTION_GET_CONTENT for user to select another image.
     */
    private class GalleryPicImageViewLongClicked implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) {
            StartBrowseGalleryIntent();
            return false;
        }
    }

    /**
     * Displays dialog asking user whether they are satisfied with the image choice.
     */
    private class ConfirmSelectionButton implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            ConfirmSelectionDialogFragment dialog = new ConfirmSelectionDialogFragment();
            dialog.show(getFragmentManager(), "ConfirmSelectionDialogFragment");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onDialogClick(DialogFragment dialog, int button) {
        if (button == ConfirmSelectionDialogFragment.CONFIRM) {
            Intent intent = new Intent(getApplicationContext(), ScribbleActivity.class);
            Uri picUri = null;
            try {
                picUri = image.generateUri();
            } catch (IOException e) {
                e.printStackTrace();
            }
            intent.putExtra("bitmap_uri", picUri.toString());
            startActivity(intent);
        }
    }

    private void StartBrowseGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void UpdateGalleryPicImageView() {
        galleryPicImageView.setImageBitmap(image.bitmap);
    }

}
