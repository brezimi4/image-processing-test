package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;

/**
 * <i>Imaginator</i> is a simple Java class which makes some of Bitmap class' methods more accessible.
 * Also implements methods to generate Bitmap uri saving it beforehand.
 */
public class Imaginator {
    private final String TAG = this.getClass().getSimpleName();
    Bitmap bitmap;
    private int[] pixels;
    private int width;
    private int height;
    private Context context;
    Bitmap.Config type;

    public Imaginator(Bitmap b, Context context) {
        bitmap = b;
        this.context = context;
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        type = bitmap.getConfig();
    }

    /**
     *
     * @param euclidianCoords
     * @param width
     * @return absolute coords for easy flatten matrix manipulation
     */
    public static int getAbsoluteCoords(final int euclidianCoords[], int width) {
        if (euclidianCoords.length != 2) {
            return -1;
        }

        return euclidianCoords[0] + euclidianCoords[1]*width;
    }

    private void saveChanges() {
        switch (bitmap.getConfig()) {
            case ARGB_8888:
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                bitmap.copyPixelsFromBuffer(IntBuffer.wrap(pixels));
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Uri generateUri() throws IOException {
        saveChanges();
        File picFile = null;
        SavingPic newPic = new SavingPic(context);
        Uri picUri = null;

        try {
            picFile = newPic.createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
            Log.w("takePic", "createImageFile failed");
        }
        // Continue only if the File was successfully created
        if (picFile != null) {
            FileOutputStream fos = new FileOutputStream(picFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            picUri = Uri.fromFile(picFile);
        } else {
            Log.w(TAG, "photoFile is null");
        }

        return picUri;
    }


    public int[] getPixels() {
        return pixels;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getType() {
        return type.toString();
    }
}
