package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

/**
 * <i>TakePicActivity</i> handles intent of taking photo and passes it to available application
 * that can handle it to process it. Then it saves the taken image and passes back its uri.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class TakePicActivity extends Activity {
    private final String TAG = this.getClass().getSimpleName();
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    File image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        image = dispatchTakePictureIntent();
    }

    private File dispatchTakePictureIntent() {
        File photoFile = null;

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            SavingPic newPic = new SavingPic(this);
            try {
                photoFile = newPic.createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.w("takePic", "createImageFile failed");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } else {
                Log.w(TAG, "photoFile is null");
            }
        } else {
            Log.w(TAG, "getPackageManager is null");
        }

        return photoFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Log.w(TAG, image.getAbsolutePath());
            if (image.exists()) {
                Uri uri = Uri.fromFile(image);
                Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
                intent.putExtra("bitmap_uri", uri.toString());
                startActivity(intent);
            } else {
                Log.w(TAG, "image does not exist");
                finish();
            }
        }
    }
}
