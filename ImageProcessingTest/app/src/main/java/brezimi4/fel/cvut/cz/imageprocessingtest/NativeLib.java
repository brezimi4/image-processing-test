package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.graphics.Bitmap;

/**
 * <i>NativeLib</i> class is a simple Java class which contains static function references
 * of their native code counterparts. This means that if possible native code functions
 * should be accessed exclusively through class.
 */
public class NativeLib {
    static {
        System.loadLibrary("color-image");
    }

    public static native int reverseRedBlue(Bitmap bitmap);
    public static native Bitmap colorImage(Bitmap bitmap, Bitmap scribbles, Bitmap alpha, int[] lables);
}
