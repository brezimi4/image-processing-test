package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import java.io.IOException;

/**
 * <i>ScribbleActivity</i> provides user with options regarding painting Scribbles. These are:
 * brush color, brush softness and brush size.
 */
public class ScribbleActivity extends AppCompatActivity{
    private final String TAG = this.getClass().getSimpleName();

    private Bitmap bitmap;
    private int brushColor;
    private int brushSize;
    private boolean brushSoft;
    private DrawingView drawing;
    private SeekBar brushSizeBar;
    private Switch softnessSwitch;
    private ImageButton undoButton;
    private TextView brushSoftnessText;
    private ImageView brushStateImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_scribble);
        drawing = (DrawingView) findViewById(R.id.drawing);

        Uri uri = Uri.parse(getIntent().getExtras().getString("bitmap_uri"));
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
        } catch (IOException e) {
            Log.e(TAG, "Bitmap with passed uri does not cannot be opened.");
            e.printStackTrace();
        }

        setup();
    }

    private void setup() {
        setupUI();
        setupDrawing();
        updateUI();
    }

    private void setupUI() {
        brushSizeBar = (SeekBar) findViewById(R.id.brushSizeBar);
        brushSizeBar.setProgress(0);
        brushSizeBar.setOnSeekBarChangeListener(new brushSizeBarChanged());
        brushSize = getResources().getInteger(R.integer.min_brush_size);

        softnessSwitch = (Switch) findViewById(R.id.softnessSwitch);
        brushSoft = false;
        softnessSwitch.setChecked(false);
        softnessSwitch.setOnCheckedChangeListener(new softnessSwitchCheckedChanged());

        brushSoftnessText = (TextView) findViewById(R.id.brushSoftnessText);

        undoButton = (ImageButton) findViewById(R.id.undoButton);
        undoButton.setOnLongClickListener(new undoButtonLongClicked());
    }

    private void updateUI() {
        brushStateImageView = (ImageView) findViewById(R.id.brushStateImageView);
        brushStateImageView.setColorFilter(brushColor);

        int maxBrushSize = getResources().getInteger(R.integer.max_brush_size);
        int padding = BasicFunctionality.computePXfromDP(this, (maxBrushSize - brushSize)/2);
        brushStateImageView.setPadding(padding, padding, padding, padding);

        brushStateImageView.setImageAlpha(brushSoft ? getResources().getInteger(R.integer.brush_soft_alpha)
                                                        : getResources().getInteger(R.integer.brush_hard_alpha));

        brushSoftnessText.setText(brushSoft ? getResources().getString(R.string.brush_soft_text)
                                                : getResources().getString(R.string.brush_hard_text));
    }

    private void setupDrawing() {
        brushColor = getResources().getColor(R.color.red);
        drawing.setPaintColor(brushColor);
        drawing.setBrushSize(BasicFunctionality.computePXfromDP(this, BasicFunctionality.normalizeBrushSize(this, brushSize)));
        drawing.setBrushSoft(brushSoft);
        drawing.setImageBitmap(bitmap);
    }

    private class brushSizeBarChanged implements SeekBar.OnSeekBarChangeListener {
        private TextView brushSizeText = (TextView) findViewById(R.id.brushSizeText);
        private Handler handler = new Handler();

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int realProgress = BasicFunctionality.normalizeBrushSize(getApplicationContext(), progress);
            int px = BasicFunctionality.computePXfromDP(getApplicationContext(), realProgress);
            drawing.setBrushSize(px);
            brushSizeText.setText("brush size: " + realProgress + "dp");
            brushSize = realProgress;
            updateUI();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            handler.removeCallbacks(makeBrushSizeTextInvisible);
            brushSizeText.setVisibility(View.VISIBLE);
        }


        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            handler.postDelayed(makeBrushSizeTextInvisible, 2000);
        }

        Runnable makeBrushSizeTextInvisible = new Runnable() {
            @Override
            public void run() {
                brushSizeText.setVisibility(View.INVISIBLE);
            }
        };
    }

    private class softnessSwitchCheckedChanged implements CompoundButton.OnCheckedChangeListener {
        private Handler handler = new Handler();
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            handler.removeCallbacks(makeSoftnessTextInvisible);
            brushSoft = isChecked;
            drawing.setBrushSoft(brushSoft);
            updateUI();
            brushSoftnessText.setVisibility(View.VISIBLE);
            handler.postDelayed(makeSoftnessTextInvisible, 2000);
        }

        Runnable makeSoftnessTextInvisible = new Runnable() {
            @Override
            public void run() {
                brushSoftnessText.setVisibility(View.INVISIBLE);
            }
        };

    }
    public void colorClicked(View view) {
        switch (view.getId()) {
            case R.id.colorRedButton:
                    brushColor = getResources().getColor(R.color.red);
                break;
            case R.id.colorOrangeButton:
                    brushColor = getResources().getColor(R.color.orange);
                break;
            case R.id.colorYellowButton:
                    brushColor = getResources().getColor(R.color.yellow);
                break;
            case R.id.colorGreenButton:
                    brushColor = getResources().getColor(R.color.green);
                break;
            case R.id.colorBlueButton:
                    brushColor = getResources().getColor(R.color.blue);
                break;
            case R.id.colorPurpleButton:
                    brushColor = getResources().getColor(R.color.purple);
                break;
            case R.id.colorPinkButton:
                    brushColor = getResources().getColor(R.color.pink);
                break;
            case R.id.colorGreyButton:
                    brushColor = getResources().getColor(R.color.grey);
                break;
        }

        drawing.setPaintColor(brushColor);
        updateUI();
    }

    public void undoButtonClicked(View view) {
        drawing.undoScribble();
    }

    private class undoButtonLongClicked implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) {
            drawing.resetScribbles();
            return false;
        }
    }

    public void redoButtonClicked(View view) {
        drawing.redoScribble();
    }

    public void processButtonClicked(View view) {
        ProgressBar nativeCodeProgressBar = (ProgressBar) findViewById(R.id.nativeCodeProgressBar);
        nativeCodeProgressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        Bitmap segmentedBitmap = drawing.getColoredBitmap();

        Log.d(TAG, "bitmap segmentation has finished");
        if (segmentedBitmap != null) {
            Log.d(TAG, "segmentedBitmap is not null");
            int width = segmentedBitmap.getWidth();
            int height = segmentedBitmap.getHeight();
            int[] pixels = new int[width*height];
            segmentedBitmap.getPixels(pixels, 0, width, 0, 0, width, height);
            int isFilled = 0;
            int index = -1;
            for (int i = 0; i < pixels.length; i++) {
                if (pixels[i] != 0) {
                    isFilled = 1;
                    index = i;
                    break;
                }
            }
            Log.d(TAG, "width=" + segmentedBitmap.getWidth() + " | height=" + segmentedBitmap.getHeight()
                    + " | format=" + segmentedBitmap.getConfig().name() + " | isFilled=" + isFilled + " | index=" + index);

        }
        drawing.setImageBitmap(segmentedBitmap);
        Log.d(TAG, "imageBitmap has been set to segmentedBitmap");

        nativeCodeProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
