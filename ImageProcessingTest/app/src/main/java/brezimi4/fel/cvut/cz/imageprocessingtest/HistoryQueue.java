package brezimi4.fel.cvut.cz.imageprocessingtest;

import java.util.LinkedList;

/**
 * <i>HistoryQueue</i> is an Java class which changes and adds some functionality of java.util.LinkedList.
 * Works as a queue that automatically throws away last member when capacity would be exceeded.
 * @param <T>
 */
public class HistoryQueue<T> {
    private int capacity;
    private LinkedList<T> memory;

    public HistoryQueue(int capacity) {
        this.capacity = capacity;
        memory = new LinkedList<T>();
    }

    public void memorize(T object) {
        if (memory.size() == capacity) {
            memory.removeLast();
        }
        memory.addFirst(object);
    }

    public T remember() {
        if (memory.isEmpty()) return null;
        T mostRecent = memory.getFirst();
        memory.removeFirst();
        return mostRecent;
    }

    public boolean isEmpty() {
        return memory.isEmpty();
    }

    public void clear() {
        memory.clear();
    }
}
