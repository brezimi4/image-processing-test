package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;

import java.util.ArrayList;

/**
 * <i>BasicFunctionality</i> class provides some useful functions for Bitmap and View manipulation.
 * This class is not to be instantiated as it implements static functions only.
 */
public class BasicFunctionality {
    public static int MAX_UINT8 = 255;

    /**
     * Turns ArrayList of Integers into simple int array.
     * @param arrayList
     * @return
     */
    public static int[] arrayListToArray(ArrayList<Integer> arrayList) {
        int[] array = new int[arrayList.size()];
        int index = 0;
        for(Integer i : arrayList) {
            array[index++] = i.intValue();
        }

        return array;
    }

    /**
     *
     * @param context
     * @param rawBrushSize
     * @return
     */
    public static int normalizeBrushSize(Context context, int rawBrushSize) {
        int minBrushSize = context.getResources().getInteger(R.integer.min_brush_size);
        int maxBrushSize = context.getResources().getInteger(R.integer.max_brush_size);

        int normalizedBrushSize = (int) (((float) (maxBrushSize - minBrushSize)/maxBrushSize)*rawBrushSize
                                            + minBrushSize);

        return normalizedBrushSize;
    }

    /**
     * Computes pixel value from density-independent pixel value using device's display parameters.
     * @param context
     * @param dp
     * @return
     */
    public static int computePXfromDP(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int px = (int) (dp * scale + 0.5f);

        return px;
    }

    /**
     * Changes color alpha channel so that the color is totally opaque.
     * @param color
     * @return
     */
    public static int getOpaqueColor(int color) {
        return Color.argb(MAX_UINT8, Color.red(color), Color.green(color), Color.blue(color));
    }
}
