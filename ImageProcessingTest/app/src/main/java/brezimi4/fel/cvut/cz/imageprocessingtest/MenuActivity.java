package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

/**
 * <h1>Image Processing Test</h1>
 * This Android application was created as a semester project for Digital Image Processing class
 * during winter semester 2018. The application implements some methods for sketch colorization
 * similar to those in TVPaint plugin LazyBrush.
 * <p>
 * Its main is to provide simple UI for the user to easily color sketches even of complex styles.
 * This is achieved with the use of min-cut/max-flow algorithm implemented using GridCut library.
 *
 * @author brezimi4
 * @version 1.0
 * @since 2018-05-15
 */

/**
 * <i>MenuActivity</i> provides simple menu consisting of <i>TakePicActivity</i> and <i>GalleryActivity</i>.
 */
public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);

        ImageButton takePicButton = (ImageButton) findViewById(R.id.takePicButton);
        takePicButton.setOnClickListener(new TakePicButtonClicked());
        ImageButton openGalleryButton = (ImageButton) findViewById(R.id.openGalleryButton);
        openGalleryButton.setOnClickListener(new OpenGalleryButtonClicked());
    }

    private class TakePicButtonClicked implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), TakePicActivity.class);
            startActivity(intent);
        }
    }

    private class OpenGalleryButtonClicked implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
            startActivity(intent);
        }
    }
}
