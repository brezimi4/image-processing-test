package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static java.lang.Math.max;

/**
 * <i>DrawingView</i> is a custom Android View serving as canvas for user to paint scribbles
 * over underlying image. After the user is satisfied with the result and clicks the process button
 * native code starts the process of colorization.
 */
public class DrawingView extends View {
    public static final int MAX_ALPHA = 255;

    private Context context;
    private ArrayList<Scribble> scribbleMap;
    private HistoryQueue<Scribble> scribbleHistory;
    private static final int capacity = 10;
    //drawing path
    private Path drawPath;
    //drawing and canvas paint
    private Paint drawPaint, canvasPaint;
    //initial color
    private int paintColor = 0xFF660000;
    //initial size
    private int brushSize = 10;
    private boolean brushSoft;
    //canvas
    private Canvas drawCanvas;
    //canvas bitmap
    private Bitmap canvasBitmap;
    //image bitmap
    private Bitmap imageBitmap;
    private boolean imageSetUp = false;
    //view parameters
    private int width;
    private int height;
    private Rect rectSrc;
    private Rect rectDest;

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupDrawing();
    }

    /**
     * Handles user's touch input and interprets it as <i>Scribble</i> object. Also adds
     * <i>Scribble</i>s to scribbleMap so that they can be drawn in further onDraw method calls.
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //detect user touch
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                scribbleMap.add(new Scribble(drawPath, drawPaint, brushSoft));
                scribbleHistory.clear();
                drawPath.reset();
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    /**
     * Draws user selected image, then draws Scribble objects over it.
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //draw view
        canvas.drawBitmap(canvasBitmap, 0, 0, null);
        if (imageSetUp) {
            canvas.drawBitmap(imageBitmap, rectSrc, rectDest, null);
        }
        for (Scribble s : scribbleMap) {
            canvas.drawPath(s.path, s.paint);
        }
        canvas.drawPath(drawPath, drawPaint);
    }

    /**
     * Adjusts canvasBitmap so that it fits in <i>DrawingView</i>.
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        //view of given size
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
        width = w;
        height = h;

        resizeImageBitmap();
    }

    /**
     * Is invoked when undoButton is clicked. Removes <i>Scribble</i> last drawn from scribbleMap
     * so that canvas is reversed to its last state.
     */
    public void undoScribble() {
        if (!scribbleMap.isEmpty()) {
            int index = scribbleMap.size() - 1;
            scribbleHistory.memorize(scribbleMap.get(index));
            scribbleMap.remove(index);
        }
        invalidate();
    }

    /**
     * Removes all <i>Scribble</i>s when undoButton is long clicked. Canvas si reset to initial state.
     */
    public void resetScribbles() {
        for (int index = max(0, scribbleMap.size() - capacity); index < scribbleMap.size(); index++) {
            scribbleHistory.memorize(scribbleMap.get(index));
        }
        scribbleMap.clear();
        invalidate();
    }

    /**
     * Adds back <i>Scribble</i> which was last undone.
     */
    public void redoScribble() {
        if (!scribbleHistory.isEmpty()) {
            scribbleMap.add(scribbleHistory.remember());
        }
        invalidate();
    }

    private void setupDrawing() {
        scribbleMap = new ArrayList<Scribble>();
        scribbleHistory = new HistoryQueue<Scribble>(capacity);
        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(false);
        drawPaint.setStrokeWidth(brushSize);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);
        canvasPaint.setColor(Color.TRANSPARENT);
    }

    /**
     * Resizes image Bitmap filling canvas as much as possible.
     */
    private void resizeImageBitmap() {
        float viewAspectRatio = (float) width/height;
        //imageSetUp = true;
        int bitmapWidth = imageBitmap.getWidth();
        int bitmapHeight = imageBitmap.getHeight();
        float bitmapAspectRatio = (float) bitmapWidth/bitmapHeight;
        int bitmapNewWidth;
        int bitmapNewHeight;
        rectSrc = new Rect();
        rectDest = new Rect();
        rectSrc.left = rectSrc.top = 0;
        rectSrc.right = bitmapWidth; rectSrc.bottom = bitmapHeight;

        if (viewAspectRatio > bitmapAspectRatio) {
            bitmapNewWidth = (int) ((float) height*bitmapAspectRatio);
            bitmapNewHeight = height;
            rectDest.left = (int) (width - bitmapNewWidth)/2; rectDest.top = 0;
            rectDest.right = rectDest.left + bitmapNewWidth; rectDest.bottom = bitmapNewHeight;
        } else {
            bitmapNewWidth = width;
            bitmapNewHeight = (int) ((float) width/bitmapAspectRatio);
            rectDest.left = 0; rectDest.top = (int) (height - bitmapNewHeight)/2;
            rectDest.right = bitmapNewWidth; rectDest.bottom = rectDest.top + bitmapNewHeight;
        }

        imageSetUp = true;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
        NativeLib.reverseRedBlue(this.imageBitmap);

        invalidate();
    }

    public void setPaintColor(int paintColor) {
        this.paintColor = paintColor;
        drawPaint.setColor(paintColor);
        drawPaint.setAlpha(brushSoft ? context.getResources().getInteger(R.integer.brush_soft_alpha)
                                        : context.getResources().getInteger(R.integer.brush_hard_alpha));
    }

    public void setBrushSize(int brushSize) {
        this.brushSize = brushSize;
        drawPaint.setStrokeWidth(brushSize);
    }

    public void setBrushSoft(boolean brushSoft) {
        this.brushSoft = brushSoft;
        drawPaint.setAlpha(brushSoft ? context.getResources().getInteger(R.integer.brush_soft_alpha)
                                        : context.getResources().getInteger(R.integer.brush_hard_alpha));
    }

    /**
     * Is invoked when image Bitmap is to be processed. Creates variables needed for native code
     * implementation of colorization algorithm. These are: <i>Scribble</i> mask,
     * <i>Scribble</i> softness mask and color labels array.
     * @return
     */
    public Bitmap getColoredBitmap() {
        Bitmap scribblesBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas scribblesCanvas = new Canvas(scribblesBitmap);
        Bitmap alphaBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
        Canvas alphaCanvas = new Canvas(alphaBitmap);
        scribblesCanvas.drawColor(Color.WHITE);
        ArrayList<Integer> colorLableList = new ArrayList<Integer>();
        for (Scribble s : scribbleMap) {
            s.paint.setAlpha(s.brushSoft ? 1 : 2);
            alphaCanvas.drawPath(s.path, s.paint);
            s.paint.setAlpha(MAX_ALPHA);
            scribblesCanvas.drawPath(s.path, s.paint);
            if (!colorLableList.contains(s.paint.getColor())) colorLableList.add(s.paint.getColor());
        }

        int[] colorLableArray = BasicFunctionality.arrayListToArray(colorLableList);

        Bitmap rScribblesBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas rScribblesCanvas = new Canvas(rScribblesBitmap);
        Bitmap rAlphaBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Bitmap.Config.ALPHA_8);
        Canvas rAlphaCanvas = new Canvas(rAlphaBitmap);
        rScribblesCanvas.drawBitmap(scribblesBitmap, rectDest, rectSrc, null);
        rAlphaCanvas.drawBitmap(alphaBitmap, rectDest, rectSrc, null);

        //start debug
        SavingPic.saveToFile("scribblesBitmap", scribblesBitmap);
        SavingPic.saveToFile("rScribblesBitmap", rScribblesBitmap);
        //end debug

        NativeLib.reverseRedBlue(rScribblesBitmap);
        return NativeLib.colorImage(imageBitmap, rScribblesBitmap, rAlphaBitmap, colorLableArray);
    }
}
