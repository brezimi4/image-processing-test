package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

/**
 * <i>ConfirmSelectionDialogFragment</i> is an Android Dialog Fragment with message, confirm button
 * and confirm button.
 */
public class ConfirmSelectionDialogFragment extends DialogFragment {
    public interface ConfirmDialogListener {
        void onDialogClick(DialogFragment dialog, int button);
    }

    /**
     * Constants passed by <i>ConfirmSelectionDialogFragment</i> when either of buttons is pressed.
     */
    public static int CONFIRM = 1;
    public static int CANCEL = 0;
    ConfirmDialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (ConfirmDialogListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement ConfirmDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.confirm_selection_message)
                .setPositiveButton(R.string.confirm_selection_positive, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogClick(ConfirmSelectionDialogFragment.this, CONFIRM);
                        ConfirmSelectionDialogFragment.this.getDialog().cancel();
                    }
                })
                .setNegativeButton(R.string.confirm_selection_negative, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogClick(ConfirmSelectionDialogFragment.this, CANCEL);
                        ConfirmSelectionDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
