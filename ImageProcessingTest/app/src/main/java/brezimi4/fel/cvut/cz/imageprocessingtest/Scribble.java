package brezimi4.fel.cvut.cz.imageprocessingtest;

import android.graphics.Paint;
import android.graphics.Path;

/**
 * <i>Scribble</i> combines two Android objects Path and Paint into one. Each <i>Scribble</i>
 * is also assigned brushSoftness attribute when constructed.
 */
public class Scribble {
    Path path;
    Paint paint;
    boolean brushSoft;

    public Scribble(Path path, Paint paint) {
        this.path = new Path(path);
        this.paint = new Paint(paint);
        this.brushSoft = false;
    }

    public Scribble(Path path, Paint paint, boolean brushSoft) {
        this.path = new Path(path);
        this.paint = new Paint(paint);
        this.brushSoft = brushSoft;
    }
}
